<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{!! trans('system.title') !!}</title>

    <!-- CSS -->

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700">
    <link rel="stylesheet" href="{!! asset('assets/bootstrap/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/font-awesome/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/elegant-icons/css/elegant-icons.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/owl-carousel/owl.carousel.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/owl-carousel/owl.theme.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/magnific-popup/magnific-popup.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!}">

    <!--Colors-->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/red.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/orange.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/green.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/lime.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/brown.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/blue.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/yellow.css') !!}"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/colors/purple.css') !!}"> -->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body data-spy="scroll" data-offset="80">

    {{-- <div class="lang-bar">
        <button class="btn-custom dropdown-toggle" type="button" data-toggle="dropdown">
            {{ strtoupper(App::getLocale()) }}</button>
        <ul class="dropdown-menu">
            @foreach(config('laravellocalization.supportedLocales') as $key=>$lang)
                <li><a href="{!! \LaravelLocalization::localizeURL('', $key) !!}">{!! strtoupper($key) !!}</a></li>
            @endforeach
        </ul>
    </div> --}}
@yield('meniu')

@yield('content')

    <!-- Contact us -->
    <div id="contact-section" class="parallax contact-container" data-stellar-background-ratio="0.3">

        <div class="inner-contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 contact">
                        <h3>{!! trans('contacts.about_us') !!}</h3>
                        <p>{!! trans('contacts.about_us_description') !!}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7 form-group contact-form">
                        <h4>{!! trans('contacts.send_message') !!}</h4>
                        <form method="post" action="">
                            <input type="text" name="email" placeholder="{!! trans('form.email') !!}" class="form-control contact-email">
                            <input type="text" name="subject" placeholder="{!! trans('form.subject') !!}" class="form-control contact-subject">
                            <textarea name="message" class="form-control" placeholder="{!! trans('form.message') !!}"></textarea>
                            <button type="submit" class="btn btn-contact">{!! trans('form.submit') !!}</button>
                        </form>
                    </div>
                    <div class="col-sm-5 contact-address contact-about">
                        <h4>{!! trans('contacts.contact_us') !!}</h4>
                        <p>{!! trans('contacts.about_us_txt') !!}</p>
                        <h4>{!! trans('contacts.address') !!}</h4>
                        <p><i class="fa fa-map-marker"></i>{!! trans('contacts.address_value') !!}</p>
                        <p><i class="fa fa-phone"></i>{!! trans('contacts.phone') !!} {!! trans('contacts.phone_value') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- footer -->
    <div id="footer">
        <h3>{!! trans('pre_footer.title') !!}</h3>
        <h4>{!! trans('pre_footer.txt') !!}</h4>
    </div>
    <!-- footer 2 -->
    <div id="footer2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="copyright">&copy;{!! trans('footer.firm_title') !!}
                        <script type="text/javascript">
                            //<![CDATA[
                            var d = new Date()
                            document.write(d.getFullYear())
                            //]]>
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <script src="{!! asset('assets/js/jquery-1.10.2.min.js') !!}"></script>
    <script src="{!! asset('assets/bootstrap/js/bootstrap.min.js') !!}"></script>
{{--    <script src="{!! asset('assets/js/jquery.backstretch.min.js') !!}"></script>--}}
{{--    <script src="{!! asset('assets/js/retina-1.1.0.min.js') !!}"></script>--}}
    <script src="{!! asset('assets/js/jquery.easing.1.3.min.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.stellar.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('assets/owl-carousel/owl.carousel.min.js') !!}"></script>
    <script src="{!! asset('assets/magnific-popup/jquery.magnific-popup.min.js') !!}"></script>
    <script src="{!! asset('assets/js/script.js') !!}"></script>
    <script src="{!! asset('assets/js/main.js') !!}"></script>


</body>

</html>

