@extends('layouts.welcome')

@section('meniu')
    @include('parts.meniu_link')
@stop

@section('content')
    <div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
        <div class="inner-bg">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1 top-description">



                    </div>
                </div>

            </div>


        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 page-content">
                <h3>{{trans('terms_of_use.title')}}</h3>
                <p>
                    {{trans('terms_of_use.p1')}}
                </p>

                <p>
                    {{trans('terms_of_use.p2_lichnye')}}
                </p>

                <p>
                    {{trans('terms_of_use.p3_lichnye')}}
                </p>


                <p>
                    {{trans('terms_of_use.p4_konf')}}
                </p>
                <p>
                    {{trans('terms_of_use.p5_konf')}}
                </p>

                <p>
                    {{trans('terms_of_use.p6_obshie')}}
                </p>
                <p>
                    {{trans('terms_of_use.p7_obshie')}}
                </p>

                <p>
                    {{trans('terms_of_use.p8_ip')}}
                </p>
                <p>
                    {{trans('terms_of_use.p9_ip')}}
                </p>

                <p>
                    {{trans('terms_of_use.p10_cookies')}}
                </p>
                <p>
                    {{trans('terms_of_use.p11_cookies')}}
                </p>

                <p>
                    {{trans('terms_of_use.p12_vneshnie')}}
                </p>
                <p>
                    {{trans('terms_of_use.p13_vneshnie')}}
                </p>

                <p>
                    {{trans('terms_of_use.p14_ezhem')}}
                </p>
                <p>
                    {{trans('terms_of_use.p15_ezhem')}}
                </p>

                <p>
                    {{trans('terms_of_use.p16_nezhelat')}}
                </p>
                <p>
                    {{trans('terms_of_use.p17_nezhelat')}}
                </p>
            </div>
        </div>
    </div>
@stop