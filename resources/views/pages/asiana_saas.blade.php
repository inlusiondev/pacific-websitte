@extends('main')

@section('meniu')
    @include('parts.meniu_link')
@stop

@section('content')
    <div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
        <div class="inner-bg">

        </div>
    </div>
    <div class="parallax asiana-container" >


        <div class="container">
            <div class="row">
                <div class="col-sm-12 team">
                    <h3>{!! trans('asiana_saas.title') !!}</h3>
                    <img src="{!! asset('assets/img/asiana_saas.jpg') !!}">
                    <h4>{!! trans('asiana_saas.slogan') !!}</h4>
                    <p>
                        {!! trans('asiana_saas.p1') !!}
                    </p>
                    <p>
                        {!! trans('asiana_saas.p2') !!}
                    </p>
                    <p>
                        <strong>{!! trans('asiana_saas.p3') !!}</strong>
                    </p>
                    <ul>
                        <li>
                            {!! trans('asiana_saas.list1') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list2') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list3') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list4') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list5') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list6') !!}
                        </li>
                        <li>
                            {!! trans('asiana_saas.list7') !!}
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

@stop
