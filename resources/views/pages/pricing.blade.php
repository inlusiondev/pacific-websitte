<div id="pricing-section" class="pricing-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pricing">
                <h3>{{ trans('pricing.heading') }}</h3>
                <p>{{ trans('pricing.sub_heading') }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 toLeft">
                <div class="price-column">
                    <h2>{{ trans('pricing.item1_title') }}</h2>
                    <h3><span>{!! trans('pricing.item1_price') !!}</span></h3>
                    <ul>
                        <li>{{ trans('pricing.feature_1') }}</li>
                        <li>{{ trans('pricing.feature_2') }}</li>
                        <li>{{ trans('pricing.feature_3') }}</li>
                        <li><button type="button" class="btn btn-top btn-lg">{{ trans('pricing.item_purchase') }}</button></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 toIn">
                <div class="price-column large">
                    <h2>{{ trans('pricing.item2_title') }}</h2>
                    <h3><span>{!! trans('pricing.item2_price') !!}</span></h3>
                    <ul>
                        <li>{{ trans('pricing.feature_1') }}</li>
                        <li>{{ trans('pricing.feature_2') }}</li>
                        <li>{{ trans('pricing.feature_3') }}</li>
                        <li>{{ trans('pricing.feature_4') }}</li>
                        <li><button type="button" class="btn btn-top btn-lg">{{ trans('pricing.item_purchase') }}</button></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 toRight">
                <div class="price-column">
                    <h2>{{ trans('pricing.item3_title') }}</h2>
                    <h3><span>{!! trans('pricing.item3_price') !!}</span></h3>
                    <ul>
                        <li>{{ trans('pricing.feature_1') }}</li>
                        <li>{{ trans('pricing.feature_2') }}</li>
                        <li>{{ trans('pricing.feature_3') }}</li>
                        <li>{{ trans('pricing.feature_4') }}</li>
                        <li>{{ trans('pricing.feature_5') }}</li>
                        <li><button type="button" class="btn btn-top btn-lg">{{ trans('pricing.item_purchase') }}</button></li>
                    </ul>

                </div>
            </div>
        </div>

    </div>

</div>