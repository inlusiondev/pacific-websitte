@extends('layouts.welcome')

@section('meniu')
    @include('parts.meniu_link')
@stop

@section('content')
    <div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
        <div class="inner-bg">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1 top-description">



                    </div>
                </div>

            </div>


        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 page-content">
                <h3>{{trans('terms_and_conds.title')}}</h3>
                <p>
                    <b>
                        {{trans('terms_and_conds.title')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p1')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p2')}}
                </p>
                <p>
                    <b>
                        {{trans('terms_and_conds.p3_b_pol_o_priv')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p4_pol_o_priv')}}
                </p>
                <p>
                    <b>
                        {{trans('terms_and_conds.p5_b_konf')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p6_konf')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p7_konf')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p8_b_iskl')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p9_iskl')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p10_iskl')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p11_iskl')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p12_iskl')}}
                </p>

                <p>
                    <b>
                    {{trans('terms_and_conds.p13_b_rast_dogovor')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p14_rast_dogovor')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p15_b_dostupnost')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p16_dostupnost')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p17_dostupnost')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p18_b_ssylki_na_sait')}}
                    </b>
                </p>
                <p>
                        {{trans('terms_and_conds.p19_ssylki_na_sait')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p20_b_ssylki_s_saita')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p21_ssylki_s_saita')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p22_b_uvedomlenie')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p23_uvedomlenie')}}
                </p>
                <p>
                    {{trans('terms_and_conds.p24_uvedomlenie')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p25_b_sviaz')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p26_sviaz')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p27_b_forsmazhor')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p28_forsmazhor')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p29_b_otkaz')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p30_otkaz')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p31_b_obshie_pol')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p32_obshie_pol')}}
                </p>

                <p>
                    <b>
                        {{trans('terms_and_conds.p33_b_uved_ob_izm')}}
                    </b>
                </p>
                <p>
                    {{trans('terms_and_conds.p34_uved_ob_izm')}}
                </p>
                <p>
                    <b>
                        {{trans('terms_and_conds.p35_b_uved_ob_izm')}}
                    </b>
                </p>
            </div>
        </div>
    </div>
@stop