@extends('main')

@section('meniu')
    @include('parts.meniu_link')
@stop

@section('content')
    <div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
        <div class="inner-bg">

        </div>
    </div>
    <div class="parallax asiana-container" >


            <div class="container">
                <div class="row">
                    <div class="col-sm-12 team">
                        <h3>{!! trans('asiana.title') !!}</h3>
                        <img src="{!! asset('assets/img/asiana.jpg') !!}">
                        <h4>{!! trans('asiana.slogan') !!}</h4>
                        <p>
                            {!! trans('asiana.p1') !!}
                        </p>
                        <p>
                            {!! trans('asiana.p2') !!}
                        </p>
                        <p>
                            <strong>{!! trans('asiana.p3') !!}</strong>
                        </p>
                        <ul>
                            <li>
                                {!! trans('asiana.list1') !!}
                            </li>
                            <li>
                                {!! trans('asiana.list2') !!}
                            </li>
                            <li>
                                {!! trans('asiana.list3') !!}
                            </li>
                            <li>
                                {!! trans('asiana.list4') !!}
                            </li>
                            <li>
                                {!! trans('asiana.list5') !!}
                            </li>
                            <li>
                                {!! trans('asiana.list6') !!}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

    </div>

@stop
