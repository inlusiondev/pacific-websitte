@extends('layouts.welcome')

@section('meniu')
    @include('parts.meniu_link')
@stop

@section('content')
    <div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
        <div class="inner-bg">
            <div class="container">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1 top-description">



                    </div>
                </div>

            </div>


        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 page-content">
                <h3>{{trans('discl.title')}}</h3>
                <p>
                    {{trans('discl.p1')}}
                </p>
                <p>
                    {{trans('discl.p2')}}
                </p>
            </div>
        </div>
    </div>
@stop