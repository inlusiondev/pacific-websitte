@extends('main')

@section('meniu')
    @include('parts.meniu_anchor')
@stop

@section('content')
        <!-- Top content -->
<div id="home" class="parallax top-content" data-stellar-background-ratio="0.3">
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                    <h2>{!! trans('top_banner.txt1') !!}</br>{!! trans('top_banner.txt2') !!}</h2>
                    <div class="description">
                    </div>
                    <div class="top-button">
                        <a class="btn btn-top btn-lg" href="#features-section"> {!! trans('top_banner.button_left') !!}</a>
                        <a class="btn btn-top-white btn-lg" href="#product-section"> {!! trans('top_banner.button_right') !!}</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Features -->
<div id="modules-section" class="features-container">
    <div class="container">
        <div class="row">
            <h3 style="color: black; text-align: center; padding: 50px;">{!! trans('modules.heading') !!}</h3>
        </div>
        <div class="row">
            <div class="col-sm-4 features-box1 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-1B.png') !!}" />
                </div>
                <h4>{!! trans('modules.title1') !!}</h4>
                <p>
                    {!! trans('modules.txt1') !!}
                </p>
            </div>
            <div class="col-sm-4 features-box2 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-2.png') !!}" />
                </div>
                <h4>{!! trans('modules.title2') !!}</h4>
                <p>
                    {!! trans('modules.txt2') !!}
                </p>
            </div>
            <div class="col-sm-4 features-box3 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-3B.png') !!}" />
                </div>
                <h4>{!! trans('modules.title3') !!}</h4>
                <p>
                    {!! trans('modules.txt3') !!}
                </p>
            </div>
            <div class="col-sm-4 features-box4 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-4.png') !!}" />
                </div>
                <h4>{!! trans('modules.title4') !!}</h4>
                <p>
                    {!! trans('modules.txt4') !!}
                </p>
            </div>
            <div class="col-sm-4 features-box5 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-5.png') !!}" />
                </div>
                <h4>{!! trans('modules.title5') !!}</h4>
                <p>
                    {!! trans('modules.txt5') !!}
                </p>
            </div>
            <div class="col-sm-4 features-box6 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-6B.png') !!}" />
                </div>
                <h4>{!! trans('modules.title6') !!}</h4>
                <p>
                    {!! trans('modules.txt6') !!}
                </p>
            </div>

            <div class="col-sm-4 features-box7 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-7.png') !!}" />
                </div>
                <h4>{!! trans('modules.title7') !!}</h4>
                <p>
                    {!! trans('modules.txt7') !!}
                </p>
            </div>

            <div class="col-sm-4 features-box8 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-8.png') !!}" />
                </div>
                <h4>{!! trans('modules.title8') !!}</h4>
                <p>
                    {!! trans('modules.txt8') !!}
                </p>
            </div>

            <div class="col-sm-4 features-box9 features-box">
                <div class="icon">
                    <img src="{!! asset('assets/img/icons/PC_icons-9B.png') !!}" />
                </div>
                <h4>{!! trans('modules.title9') !!}</h4>
                <p>
                    {!! trans('modules.txt9') !!}
                </p>
            </div>

        </div>
    </div>
</div>


<!-- Product -->

<div id="features-section" class="product-container product-bg1">
    <div class="container">
        <div class="row">

            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <div class="product">
                    <h3>{!! trans('features.title') !!}</h3>
                    <p>{!! trans('features.description') !!}</p>

                    <img src="{!! asset('assets/img/3.png') !!}" alt="">

                </div>

            </div>
        </div>
    </div>
</div>


<!-- Product -->
<div class="product-container product-bg2">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="productbox">
                    <div>
                        <i class="fa fa-send"></i>
                    </div>
                    {!! trans('features.txt1') !!}
                </div>
                <div class="productbox">
                    <div>
                        <i class="fa fa-cloud-download"></i>
                    </div>
                    {!! trans('features.txt2') !!}
                </div>

            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="productbox">
                    <div>
                        <i class="fa fa-leaf"></i>
                    </div>
                    {!! trans('features.txt3') !!}
                </div>
                <div class="productbox">
                    <div>
                        <i class="fa fa-tachometer "></i>
                    </div>
                    {!! trans('features.txt4') !!}
                </div>
            </div>
        </div>
    </div>


    <!-- Team -->
    <div id="product-section" class="parallax team-container" data-stellar-background-ratio="0.3">

        <div class="inner-team">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 team">
                        <h3>{!! trans('products.title') !!}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-md-offset-3 col-lg-offset-3">
                        <div class="team-member">
                            <a href="{!! route('asiana') !!}" class="circle-text">
                                <span>{!! trans('asiana.title') !!}</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="team-member">
                            <a href="{!! route('asiana_saas') !!}" class="circle-text">
                                <span>{!! trans('asiana_saas.title') !!}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--@include('pages.pricing');--}}

    <!-- Testimonials -->
    <div class="container" id="testimonials-section">
        <div class="row">
            <div class="col-sm-12 testimonials">
                <h3>{!! trans('testimonials.title') !!}</h3>

                <div class="col-md-12 text-center">
                    <div id="testimonial-carousel" class="owl-carousel owl-spaced">
                        <div>
                            <img src="{!! asset('assets/img/t1.jpg') !!}" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                {!! trans('testimonials.txt1') !!}
                            </h4>
                            <p>-{!! trans('testimonials.author1') !!}</p>
                        </div>
                        <div>
                            <img src="{!! asset('assets/img/t2.jpg') !!}" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                {!! trans('testimonials.txt2') !!}
                            </h4>
                            <p>-{!! trans('testimonials.author2') !!}</p>
                        </div>
                        <div>
                            <img src="{!! asset('assets/img/t3.jpg') !!}" alt="" class="img-responsive img-thumbnail" />

                            <h4>
                                {!! trans('testimonials.txt3') !!}
                            </h4>
                            <p>-{!! trans('testimonials.author3') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop