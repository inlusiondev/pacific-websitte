@section('meniu')
    <div class="navbar navbar-default navbar-fixed-top menu-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#home"></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home">{!! trans('meniu.home') !!}</a></li>
                    <li><a href="#modules-section">{!! trans('meniu.modules') !!}</a></li>
                    <li><a href="#features-section">{!! trans('meniu.features') !!}</a></li>
                    <li><a href="#product-section">{!! trans('meniu.product') !!}</a></li>
                    {{--<li><a href="#pricing-section">{!! trans('meniu.pricing') !!}</a></li>--}}
                    <li><a href="#contact-section">{!! trans('meniu.contact') !!}</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img height="15" src="{!! asset('assets/img/flags/'.LaravelLocalization::setLocale().'.jpg') !!}"><span class="caret"></span></a>
                    <ul class="dropdown-menu lang-menu" role="menu" style="width: 40px;">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li><a style="color: black !important;" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}"><img height="15" style="padding-right: 15px;" src="{!! asset('assets/img/flags/'.$localeCode.'.jpg') !!}">{!! strtoupper($localeCode) !!}</a></li>
                        @endforeach
                    </ul>
                </li>

                </ul>
            </div>
        </div>
    </div>
@stop