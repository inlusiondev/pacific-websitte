@section('meniu')
    <div class="navbar navbar-default navbar-fixed-top menu-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{Route('index')}}#home"></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{Route('index')}}#home">{!! trans('meniu.home') !!}</a></li>
                    <li><a href="{{Route('index')}}#features-section">{!! trans('meniu.features') !!}</a></li>
                    <li><a href="{{Route('index')}}#video-section">{!! trans('meniu.modules') !!}</a></li>
                    <li><a href="{{Route('index')}}#team-section">{!! trans('meniu.product') !!}</a></li>
                    {{--<li><a href="{{Route('index')}}#team-section">{!! trans('meniu.pricing') !!}</a></li>--}}
                    <li><a href="{{Route('index')}}#contact-section">{!! trans('meniu.pricing') !!}</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <img height="15" src="{!! asset('assets/img/flags/'.LaravelLocalization::setLocale().'.jpg') !!}"><span class="caret"></span></a>
                        <ul class="dropdown-menu lang-menu" role="menu" style="width: 40px;">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li><a style="color:black !important;" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}"><img height="15" style="padding-right: 15px;" src="{!! asset('assets/img/flags/'.$localeCode.'.jpg') !!}">{!! strtoupper($localeCode) !!}</a></li>
                            @endforeach

                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop