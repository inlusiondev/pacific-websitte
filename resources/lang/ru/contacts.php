<?php

return array (
    'contact_us' => 'Свяжитесь с нами сегодня',
    'about_us_description' => 'Pacific Club Limited – международная IT компания, сумевшая объединить талантливых и креативных специалистов в единую команду. Основной целью которой является вывод Вашего бизнеса на совершенно новый уровень. Наши продукты помогают привлечь новых клиентов, повышают качество работы сотрудников, увеличивают продажи и грамотно распределяют информационные ресурсы компании.',
    'send_message' => 'Сообщение',
    'about_us' => 'О нас',
    'about_us_txt' => 'Pacific Club Limited (Company Nr : ‪1576564)‬',
    'address' => 'Адрес:',
    'address_value' => 'Unit 1010, 10/F, Miramar Tower, 
132 Nathan Road, 
Tsim Sha Tsui, Kowloon, 
Hong Kong
',
    'phone' => 'Phone:',
    'phone_value' => '852 - 8198 0004‬‬‬‬‬‬‬‬‬‬‬‬',

);
