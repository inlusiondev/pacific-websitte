<?php

return array (
  'title' => 'Цена может меняться. ',
  'description' => 'Уточняйте окончательную цену у менеджера.',
  'col1_title' => 'Columbina IT',
  'col1_from' => 'От 1 751 750 рублей',
  'col1_opt1' => 'Columbina IT DEV SME 50 пользователей',
  'col1_opt2' => 'Columbina IT DEV PRO 500 пользователей',
  'button_buy' => 'Заказать',
  'col2_title' => 'Columbina ',
  'col2_from' => 'От 150 150 рублей',
  'col2_opt1' => 'Columbina Nano Business (Бизнес) 5 пользователей',
  'col2_opt2' => 'Columbina Nano PRO (Профессиональная) 50 пользователей',
  'col3_title' => 'Olivia SaaS Business',
  'col3_from' => 'От 4 247 243 рублей    ',
  'col3_opt3' => 'USER 36M LICENSE',
  'col4_title' => 'Olivia SaaS Corporate &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
  'col4_from' => 'От 7 252 245 рублей',
  'col4_opt1' => 'USER 12 Mес. LICENSE',
  'col4_opt2' => 'USER 24 Mес. LICENSE',
  'col4_opt3' => 'USER 36 Mес. LICENSE',
  'col5_title' => 'Встраиваемый модуль',
  'col5_from' => 'От 45 045,00 рублей   ',
  'col5_opt1' => 'End-to-End Encryption Tool',
  'col6_title' => 'Встраиваемый модуль',
  'col6_from' => 'От 23 523,50 рублей',
  'col6_opt1' => 'Backup and Recovery Tool ',
  'col3_opt1' => 'USER 12M LICENSE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
  'col3_opt2' => 'USER 24M LICENSE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
);
