<?php

return array (
  'home' => 'Главная',
  'features' => 'Характеристики',
  'modules' => 'Модули',
  'product' => 'Продукты',
  'pricing' => 'Цены',
  'contact' => 'Контакты',
);
