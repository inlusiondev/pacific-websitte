<?php

return array (
  'home' => 'Home',
  'features' => 'Features',
  'modules' => 'Modules',
  'product' => 'Products',
  'pricing' => 'Pricing',
  'contact' => 'Contacts',
);
