<?php
return [
    'heading' => 'Pricing',
    'sub_heading'   =>  'Choose package to buy!',
    'item1_title'   =>  'Standard',
    'item2_title'   =>  'Premium',
    'item3_title'   =>  'Ultimate',
    'item_purchase' =>  'Purchase',
    'item1_price'   =>  'From</span> $45<span>.99 per month',
    'item2_price'   =>  'From</span> $75<span>.00 per month',
    'item3_price'   =>  'From</span> $99<span>.99 per month',
    'feature_1'     =>  'Asiana CRM',
    'feature_2'     =>  'Asiana SaaS',
    'feature_3'     =>  'Analytics',
    'feature_4'     =>  'Cloud storage',
    'feature_5'     =>  'Free 24/7 Support',

];
