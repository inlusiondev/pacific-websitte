<?php

return array (
  'title' => 'Testimonials',
  'txt1' => 'Asiana SaaS allowed me to integrate all setting in web interface by myself. Our company’s expenses significantly decreased.',
  'author1' => 'János Varga, IT Director',
  'txt2' => 'We purchased Asiana CRM. The system is easy to use and work with. It helped us to keep existing customers and attract new ones shortly.',
  'author2' => 'Mária Novák, CEO',
  'txt3' => 'Due to Asiana SaaS there is no implementation period. You don’t need to have special technical skills to set up the system and work with.',
  'author3' => 'Zoltán Lakatos, IT developer.',
);
