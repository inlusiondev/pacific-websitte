<?php

return array (
  'title' => 'Features',
  'description' => 'Our products increase the quality of working with your clients, simplifying database access and speeding information flows.',
  'txt1' => 'Proprietary framework',
  'txt2' => 'Digital data system',
  'txt3' => 'Various CRM and ERP modifications',
  'txt4' => 'Cloud storage architecture',
);
