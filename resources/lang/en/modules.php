<?php

return array (
  'heading' => 'Connectable product modules',
  'title1' => 'Support for multiple languages',
  'txt1' => 'Automatic document translation for supported languages. Integration of extra languages not included in core suite.',
  'title2' => 'Calendar',
  'txt2' => 'Task reminders.',
  'title3' => 'Mail',
  'txt3' => 'Communication module - phone, e-mail.',
  'title4' => 'Agreements',
  'txt4' => 'List of current and future agreements.',
  'title5' => 'Clients',
  'txt5' => 'Client database.',
  'title6' => 'Support',
  'txt6' => 'Tasks, communication.',
  'title7' => 'Employees',
  'txt7' => 'Work plan, sales, analytics, reports.',
  'title8' => 'Products',
  'txt8' => 'Asiana CRM  Asiana SaaS.',
  'title9' => 'Extendable functionality',
  'txt9' => 'Install new modules easily.',
);
