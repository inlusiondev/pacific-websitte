<?php

return array (
  'email_error' => 'Invalid E-mail address',
  'email_success' => 'E-mail valid',
  'email' => 'E-mail',
  'subject' => 'Subject',
  'message' => 'Message',
  'submit' => 'Send',
);
