<?php

return array (
  'title' => 'Asiana SaaS',
  'slogan' => 'Attract new clients',
  'p1' => 'For many companies today it is crucial to have access to all documents and other work related information at all time and having an automated business is no less important. Due to this need, Asian SaaS is in high demand worldwide among small and medium sized businesses. It offers the technology to sell information products over the internet.',
  'p2' => 'The main advantage of Asiana is the absence of capital investment into installing, updating and servicing the servers and related software.',
  'list1' => 'The system is more secure than programs installed on a computers hard drive.',
  'list2' => 'The absence of a need to install separate programs on each individual staff computer, because access to the program is gained via the internet.',
  'list3' => 'Reducing investment into technical support and system updates.',
  'list4' => 'The ability to add and connect extended features and modules.',
  'list5' => 'Support for geographically separated businesses',
  'list6' => 'Automatic backup',
  'p3' => 'Asiana SaaS capabilities:',
  'list7' => 'Mobile access',
);
