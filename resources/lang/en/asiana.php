<?php

return array (
  'title' => 'Asiana CRM',
  'slogan' => 'A new reality of doing business',
  'p1' => 'Progres has created a new reality, a simple product offering does not guarantee demand for products any longer. Widespread adverting and low prices do not guarantee successful sales. Professional services are the new key to success.',
  'p2' => 'By using Asiana CRM, you receive a suite of instruments, that give you the ability to control sales, increase the quality of services offered, attract new clients and automate work. With Asiana you will outperform your competitors, save resources, time and money.',
  'list1' => 'Business process automation',
  'list2' => 'Client information management',
  'list3' => 'Sales management',
  'list4' => 'Time management',
  'list5' => 'Document processing automation',
  'list6' => 'Optimizing internal communications',
  'p3' => 'Asiana CRM capabilities:',
);
