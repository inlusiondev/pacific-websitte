<?php

return array (
  'contact_us' => 'Contacts',
  'about_us_description' => 'Pacific Clb Limited is an international IT company, that could unite talented and creative specialists into a single team. Our main mission is to help your business achieve the next level. Our products will help you attract new clients, increase staff productivity, grow sales and effectively allocate your information resources.',
  'send_message' => 'Message',
  'about_us' => 'About Us',
  'about_us_txt' => 'Pacific Club Limited (Company Nr : ‪1576564)‬',
  'address' => 'Address:',
  'address_value' => 'Unit 1010, 10/F, Miramar Tower, 
132 Nathan Road, 
Tsim Sha Tsui, Kowloon, 
Hong Kong
',
  'phone' => 'Phone:',
  'phone_value' => '852 - 8198 0004‬‬‬‬‬‬‬‬‬‬‬‬',
);
