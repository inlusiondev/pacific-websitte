<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function render_page($name)
    {
        return view('ajax.'.$name);
    }

    public function send_mail(Request $request)
    {
        $result = array();
        if ($request->ajax() && $request->isMethod('post')) {
            $result['error'] = 1;
            $result['txt'] = trans('form.email_error');
            if ($request->input('email') && $request->input('subject') && $request->input('message')) {
                $to = "maxim@inlu.net";
                $subject = $request->input('subject');
                $txt = $request->input('message');
                $headers = "From: ".$request->input('email');
               if(mail($to,$subject,$txt,$headers)){
                   $result['error'] = 0;
                   $result['txt'] = trans('form.email_success');
               }
            }
            return json_encode($result);
        }
    }
}
