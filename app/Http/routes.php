<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Sentinel::authenticate($credentials);

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ],
    function()
    {

    Route::get('/discl', function () {
        return view('pages.discl');
    })->name('discl');

    Route::get('/terms_and_conds', function () {
        return view('pages.terms_and_conds');
    })->name('terms_and_conds');

    Route::get('/terms_of_use', function () {
        return view('pages.terms_of_use');
    })->name('terms_of_use');

    Route::get('', function () {
        return view('pages.index');
    })->name('index');

    Route::get('/asiana', function () {
        return view('pages.asiana');
    })->name('asiana');

    Route::get('/asiana_saas', function () {
        return view('pages.asiana_saas');
    })->name('asiana_saas');

    Route::any('/ajax/{name}', 'AjaxController@render_page')->name('ajax.render');
    Route::post('sendmail', 'AjaxController@send_mail')->name('ajax.send_mail');

});
