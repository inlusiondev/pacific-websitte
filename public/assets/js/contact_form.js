jQuery(document).ready(function() {

    $(".btn-contact").click(function (e) {

        var url = $(this).data('url');
        var email = $('input[name=email]').val();
        var subject = $('input[name=subject]').val();
        var message = $('textarea[name=message]').val();

        console.log('email = '+email+' ssubject = '+subject+' message = '+message);
        $.post(url,{email : email, subject: subject, message: message}, function(json) {
            var data =  $.parseJSON(json);
            if(!data.error) {
                $('.form-result').removeClass('form-success');
                $('.form-result').addClass('form-error');
            }else{
                $('.form-result').removeClass('form-error');
                $('.form-result').addClass('form-success');
            }
            $('input[name=email]').val('');
            $('input[name=subject]').val('');
            $('textarea[name=message]').val('');
            $('.form-result').html(data.txt);
        });
        e.preventDefault();

    });

});